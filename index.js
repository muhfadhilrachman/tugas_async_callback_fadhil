const getData = (callback) => {
  fetch("https://jsonplaceholder.typicode.com/users")
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      callback(data);
    })
    .catch((error) => console.error(error));
};

getData((data) => {
  new gridjs.Grid({
    columns: ["Name", "username", "email", "phone"],
    data: data,
  }).render(document.getElementById("app"));
});
